### Capçaleres
# Capçalera 1
## Capçalera 2
### Capçalera 3
#### Capçalera 4
##### Capçalera 5


### Format de text
* _Això està escrit en cursiva_
* **Això està en negreta**
* **_Això està en negreta i cursiva_**
* ~~Això està taxat~~

### Taules
* Ara introduim una taula
  Blancs | Blaus
  --|--
  Estirada de corda | Rajoles  
  Construcció de gegants | Descans  

## Llistes
### Llista ordenada amb subapartats
1. Colors
2. Blau
3. Groc
4. Persones
   1. Homes
   2. Dones

### Llista no ordenada amb subllistes
* Colors
  - Blau
  - Groc
* Persones
  - Homes
  - Dones

### Llista "Checklist"
* [ ] Colors
  - [ ] Blau
  - [ ] Groc
* [x] Persones
  - [ ] Homes
  - [x] Dones

### Cites
Com va dir Confucio:
 >Si un problema te solució, per que et preocupes? Si un problema no té solució, pèr que et preocupes?

### Enllaços a recursos externes (links)
##### [ Click aqui per anar a google][0508f45d].Això es un text normal

#### Enllaços a imatges
![image](https://blog.irontec.com/wp-content/uploads/2019/11/gitlab-logo-gray-rgb-300x132.png)

## Introduir codi
Codi en la mateixa linea de text `var code =1;`

* Aqui teniu un tros de codi en python:
~~~
x=1
if x==1:
  #idented four spaces
  print("x is 1.")
~~~

  [0508f45d]: https://www.google.es/ "google"
